const express= require('express');
const app = express();
const http = require('http').Server(app)
const io = require('socket.io')(http);
const mongoose = require('mongoose');
var bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}))
app.use(express.static(__dirname));
const channels = require('../models/channels.js');
const channelsMessage = require('../models/channelsMessage.js');
const UserData = require('../models/user.js');

//get messages
const router = express.Router();

router.get('/channels',(req,res)=>{
    if(req.session.user){
        var channelsList=[]
   
        var Users
        var channelsList
         run().catch(error => console.log(error.stack));
         async function run() {
              Users= await UserData.find()
              var usersList=[]
              for(var i=0;i<Users.length;i++){
                 usersList.push(Users[i].username)
              }
              console.log(Users)
              console.log(usersList)
              channelsList= await channels.find()
              res.render('channels.ejs', {
                 page:'Вход', 
                 menuId:'none',
                 name:req.session.user,
                 channelsList:channelsList,
                 usersList:usersList ,
                 channel:undefined
             });
             
        }
    
    }else{
        res.render('login.ejs', {page:'Вход', menuId:'none',name:req.session.user,channel:undefined });
    }
   
    
    
    
    
})

router.post('/createNewChannel', function(req, res, next) {
        run().catch(error => console.log(error.stack));
        async function run() {
            var status=false
            var chanelData=Object.values(req.body)
            var chanelName=chanelData[0];
            var chanelUsers=chanelData[1]
            //res.render('chat.ejs', {page:'Chat', menuId:'chat',name:req.session.user });
            const  channelsList= await channels.findOne({name:chanelName},function(err,docs){
               
                if(docs){
                    status=true
                }
            
            })
            console.log(status)
            if(!status){
                var newChannelsMessage=new channelsMessage({channels:chanelName,messages:[]});
                var newChannel=new channels({name:chanelName,users:chanelUsers});
                newChannel.save() 
                newChannelsMessage.save()
                console.log(chanelData)
                res.send({
                    retStatus : 'Success',
                });
            }
            
        }
    
});
router.get('/channels/:id',function(req, res,next){
    console.log('Начал')
    if(req.session.user){
        run().catch(error => console.log(error.stack));
        var status=false
        async function run() {
            const  channelsList= await channels.findOne({name:req.params.id},function(err,docs){
                console.log(docs.users)
                if(err)
                send(500);
                if(docs.users.includes(req.session.user)||docs.users[0]=="all"){
                    console.log('прошел')
                    status=true
                }
            
            })
        // req.session.user
            
        if(status){
            
            res.render('chat.ejs', {page:'Chat', menuId:'chat',name:req.session.user ,channel:req.params.id});
            
        } else{
            res.render('login.ejs', {page:'Вход', menuId:'none',name:req.session.user,channel:undefined });
        }

        }
    }else{
        res.render('login.ejs', {page:'Вход', menuId:'none',name:req.session.user,channel:undefined });
    }
    

});
module.exports=router;