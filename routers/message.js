const express= require('express');
const app = express();
const http = require('http').Server(app)
const io = require('socket.io')(http);
const mongoose = require('mongoose');
const UserMassage = require('../models/message.js');

const bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}))
app.use(express.static(__dirname));
//get messages
const router = express.Router();

module.exports = router