//const jwt = require('jsonwebtoken');
//const User = require('../models/User');
//
//const auth= async(req,res,next ) => {
//    const token = req.header('Authorization').replace('Bearer ','');
//    const data =jwt.verify(token,process.env.JWT_KEY);
//    try{
//        const user=await user.findOne({_id:data._id,'tokens.token':tokem});
//        if(!user){
//            throw new Error()
//        }
//        req.user = user;
//        req.token =token;
//        next();
//    } catch (error) {
//        res.status(401).send({ error: 'Not authorized to access this resource' })
//    }
//};
//module.exports = auth;

var passport = require('passport');
var Strategy = require('passport-local').Strategy;

passport.use(new Strategy(
  function(username, password, cb) {
    db.users.findByUsername(username, function(err, user) {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      if (user.password != password) { return cb(null, false); }
      return cb(null, user);
    });
  }));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  db.users.findById(id, function (err, user) {
    if (err) { return cb(err); }
    cb(null, user);
  });
});