var express= require('express');
var app = express();
var mongoose = require('mongoose');
var dbUrl = 'mongodb://localhost:27017/db';
var Schema = mongoose.Schema;
var channelsSchema = new Schema({
    name: String,
    users: Array
});
var channels = mongoose.model('channels',channelsSchema)

module.exports=channels;